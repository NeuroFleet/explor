from reactor.shortcuts import *

################################################################################

@Reactor.wamp.register_middleware('fleet.storage.Local.POSIX')
class POSIX(Reactor.wamp.Nucleon):
    @Reactor.wamp.register_method(u'<host>.fs.posix.ls')
    def exec_ls(self, path):
        return [x for x in  os.listdir(path)]

    #***************************************************************************

    @Reactor.wamp.register_method(u'<host>.vfs.posix.mv')
    def exec_mv(self, source, target):
        return self.shell('mv', source, target)

    #***************************************************************************

    @Reactor.wamp.register_method(u'<host>.fs.posix.rm')
    def exec_rm(self, path):
        return self.shell('rm', path)

