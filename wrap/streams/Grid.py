from reactor.shortcuts import *

import ipfsapi

################################################################################

@Reactor.wamp.register_middleware('fleet.storage.Local.IPFS')
class IPFS(Reactor.wamp.Nucleon):
    def on_init(self, details):
        self.proxy = ipfsapi.Client(host='localhost', port=5001, base='api/v0', default_enc='json')

    #***************************************************************************

    @Reactor.wamp.register_topic(u'<host>.fs.ipfs.connect')
    def shell_commands(self, args, command, stdout):
        print("[{}] <{}> [STDOUT] {}".format(datetime.now(), 'shell', command, stdout))

    ############################################################################

    @Reactor.wamp.register_method(u'<host>.fs.ipfs.info')
    def exec_info(self):
        return self.proxy.id(peer=None)

    #***************************************************************************

    @Reactor.wamp.register_method(u'<host>.fs.ipfs.refs')
    def exec_refs(self, path):
        return self.proxy.refs(path)

    ############################################################################

    @Reactor.wamp.register_method(u'<host>.fs.ipfs.get')
    def exec_get(self, path):
        return self.proxy.get(path)

    #***************************************************************************

    @Reactor.wamp.register_method(u'<host>.fs.ipfs.cat')
    def exec_cat(self, path):
        return self.proxy.cat(path)

    #***************************************************************************

    @Reactor.wamp.register_method(u'<host>.fs.ipfs.ls')
    def exec_ls(self, path):
        return self.proxy.ls(path)

    #***************************************************************************

    @Reactor.wamp.register_method(u'<host>.fs.ipfs.mv')
    def exec_mv(self, source, target):
        return self.proxy.mv(source, target)

    #***************************************************************************

    @Reactor.wamp.register_method(u'<host>.fs.ipfs.rm')
    def exec_rm(self, path):
        return self.proxy.rm(path)

