from reactor.shortcuts import *

################################################################################

from mega import Mega

@Reactor.wamp.register_middleware('fleet.storage.Cloud.Mega')
class MegaNZ(Reactor.wamp.Nucleon):
    def on_init(self, details):
        self.wrap = Mega({
            'verbose': True,
        })
        self.conn = mega.login(email, password)

    #***************************************************************************

    @Reactor.wamp.register_topic(u'<host>.fs.mega.connect')
    def shell_commands(self, args, command, stdout):
        print("[{}] <{}> [STDOUT] {}".format(datetime.now(), 'shell', command, stdout))

    ############################################################################

    @Reactor.wamp.register_method(u'<host>.fs.mega.space')
    def get_space(self):
        return m.get_storage_space(kilo=True)

    #***************************************************************************

    @Reactor.wamp.register_method(u'<host>.fs.mega.quota')
    def get_quota(self):
        return self.conn.get_quota()

    ############################################################################

    @Reactor.wamp.register_method(u'<host>.fs.mega.ls')
    def exec_ls(self, path):
        node = None

        if path.startswith('/inbox/'):
            node = self.conn.get_node_by_type(3)
        elif path.startswith('/trash/'):
            node = self.conn.get_node_by_type(4)
        else:
            node = self.conn.get_node_by_type(2)

        return [
            (k,v)
            for k,v in  self.conn.get_files_in_node(node).iteritems()
        ]

    #***************************************************************************

    @Reactor.wamp.register_method(u'<host>.vfs.mega.mv')
    def exec_mv(self, source, target):
        return self.shell('mv', source, target)

    #***************************************************************************

    @Reactor.wamp.register_method(u'<host>.fs.mega.rm')
    def exec_rm(self, path):
        return self.shell('rm', path)

