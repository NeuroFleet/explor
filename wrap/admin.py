from django.contrib import admin

# Register your models here.
from .models import *

################################################################################

class MegaInline(admin.TabularInline):
    model = IdentityMEGA

#*******************************************************************************

class IpfsInline(admin.TabularInline):
    model = IdentityIPFS

################################################################################


