from nucleon.explor.shortcuts import *

import fs

VFS_PROVIDERs = (
    ('MEM',  "Memory FS"),
    ('OS',   "Native FS"),
    ('TEMP', "Temporary FS"),

    ('TAR',  "TAR archive"),
    ('ZIP',  "ZIP archive"),

    ('S3',   "S3 bucket"),
    ('FTP',  "FTP share"),

    ('MEGA', "Mega.nz account"),
)

################################################################################

@Reactor.orm.register_model('identity_fs')
class IdentityFileSystem(models.Model):
    owner    = models.ForeignKey(Identity, related_name='vfs')
    alias    = models.CharField(max_length=48, blank=True)

    provider = models.CharField(max_length=48, choices=VFS_PROVIDERs)
    path     = models.CharField(max_length=256)

    host     = models.CharField(max_length=128, blank=True)
    port     = models.PositiveIntegerField(default=0)

    username = models.CharField(max_length=64, blank=True)
    password = models.CharField(max_length=64, blank=True)

    @property
    def endpoint(self):
        if not hasattr(self, '_ep'):
            setattr(self, '_ep', None)

        if self._ep is None:
            if self.provider=='MEM':
                self._ep = fs.memoryfs.MemoryFS()
            elif self.provider=='TEMP':
                self._ep = fs.tempfs.TempFS(self.path, auto_clean=True, ignore_clean_errors=True)
            elif self.provider=='OS':
                self._ep = fs.osfs.OSFS(self.path, create=True)

            elif self.provider=='TAR':
                self._ep = fs.tarfs.TARFS(self.path, write=True)
            elif self.provider=='ZIP':
                self._ep = fs.zipfs.ZipFS(self.path, write=True)

            elif self.provider=='FTP':
                if self.port==0:
                    self.port = 21

                self._ep = fs.ftpfs.FTPFS(self.host, port=self.port, user=self.username, passwd=self.password)

        return self._ep

